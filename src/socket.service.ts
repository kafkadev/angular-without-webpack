import { Injectable } from '@angular/core';
//import { Observable } from 'rxjs/Observable'
import { Http, Response} from '@angular/http';
import 'rxjs/add/operator/map';
import { Socket } from 'ng-socket-io';
//const DATA_URL = 'ws://apex.redsoftnv.com/api/live/?active=1&status=started';
const SOCKET_URL = 'https://live.redsoftnv.com';
const FIRST_DATA_URL = 'https://apex.redsoftnv.com/api/live/?active=1&status=started';
const FIRST_DATA_URL2 = 'http://fakebackend.dev/first';
const TOTAL_DATA_URL = 'http://apex.redsoftnv.com/api/prematch/?hours=168';
const TOTAL_DATA_URL_NEW = 'http://adsdemo.dev/api/redsoft/category';
const SINGLE_DATA_URL = 'https://apex.redsoftnv.com/api/live/?details=';
@Injectable()
export class SocketService {
  liste : any
  constructor(private http: Http, private socket: Socket) {}
  getFirstData(){
      /*return this.http.get(FIRST_DATA_URL)
      .map(res => res.json())
      .subscribe(data => {console.log(data)});*/

      return this.http.get(FIRST_DATA_URL).map((response: Response) => response.json());
    }

    getSingleId(single_id){
      return this.http.get(SINGLE_DATA_URL + single_id).map((response: Response) => response.json());
    }


    getTotalData(){
      /*
      return this.http.get(FIRST_DATA_URL)
      .map(res => res.json())
      .subscribe(data => {console.log(data)});
      */

      return this.http.get(TOTAL_DATA_URL).map((response: Response) => response.json());
    }
    getSocket() {

      return this.socket
      .fromEvent<any>("live")
      .map(res => res
        // datayı burada işlemeyi dene !!
      /*
      {
         this.liste[data.data[0].details.matchid] = data.data[0].details
         console.log()
       }
       */
       );


    }

    sendMessage(msg: string) {
      this.socket
      .emit('type', 'betting');
    }

    close() {
      this.socket.removeAllListeners('live')
      //this.socket.disconnect()
    }
  }

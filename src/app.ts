//our root app component
import {Component, VERSION} from '@angular/core'
import {BrowserModule} from '@angular/platform-browser'

@Component({
  selector: 'my-app',
templateUrl: '/src/app.component.html'
})
export class AppComponent {
  name:string;
  constructor() {
    this.name = `Angular! v${VERSION.full}`
  }
}

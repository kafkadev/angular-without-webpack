import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';
import { SocketIoModule, SocketIoConfig } from 'ng-socket-io';
//Components
import { AppComponent } from './app';
import { LiveComponent } from './live.component';
import { SportsComponent } from './sports.component';
//Components

import { SocketService } from './socket.service';


//https://github.com/bougarfaoui/ng-socket-io
const config : SocketIoConfig = { url: 'https://live.redsoftnv.com',
options: {}};

//Routes
const appRoutes: Routes = [
  { path: '', component: SportsComponent },
  { path: 'live', component: LiveComponent }
];




@NgModule({
  imports: [ BrowserModule,
  HttpModule,
  RouterModule.forRoot(appRoutes, { useHash: true }),
  SocketIoModule.forRoot(config)
  ],
  declarations: [
  AppComponent,
  SportsComponent,
  LiveComponent
   ],
  providers: [SocketService],
  bootstrap: [ AppComponent ]
})
export class AppModule {}


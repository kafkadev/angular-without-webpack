import { Component} from '@angular/core';
import { SocketService } from './socket.service';
@Component({
  selector: 'live',
  templateUrl: '/src/live.component.html',
  styleUrls: ['/src/app.component.css']
})
export class LiveComponent{
  listeDetay : any = {}
  newIds : any[] = []
  totalMarket : any = 0
  indexVirtual : number = 0
  constructor(private socketService : SocketService) {
    this.sendMsg('live')
    this.connectSocketNew()
  }

  connectSocketNew(){

    this.socketService
    .getSocket()
    .subscribe(msg => {
      console.log(msg)
    })
  }

  sendMsg(msg){
    this.socketService.sendMessage(msg)
  }

}
//https://stackoverflow.com/questions/42978082/what-is-let-in-angular-2-templates
